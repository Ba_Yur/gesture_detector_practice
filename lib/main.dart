import 'package:flutter/material.dart';
import 'package:gesture_detector_practice/painter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double deltaX = 0.0;
  double containerHeight = 100.0;

  @override
  Widget build(BuildContext context) {
    double sliderWith = MediaQuery.of(context).size.width * 0.75;
    double sliderPercetage = 100 / sliderWith * deltaX;
    return Scaffold(
      appBar: AppBar(
        title: Text('Gesture Detector and Custom Painter'),
      ),
      body: Center(
        child: GestureDetector(
          onHorizontalDragUpdate: (DragUpdateDetails delta) {
            if (sliderPercetage < 0) {
              setState(() {
                deltaX = 0;
              });
            } else if (deltaX > sliderWith) {
              setState(() {
                deltaX = sliderWith;
              });
            } else {
              setState(() {
                deltaX += delta.delta.dx;
                if (deltaX < 0) {
                  deltaX = 0;
                } else if (deltaX > sliderWith) {
                  deltaX = sliderWith;
                }
              });
            }
            print(delta.delta.dx);
            print(deltaX);
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                sliderPercetage.toStringAsFixed(1),
                style: TextStyle(fontSize: 40),
              ),
              Container(
                width: sliderWith,
                height: containerHeight,
                child: CustomPaint(
                  painter: SliderPainter(deltaX, containerHeight, sliderWith),
                  child: Container(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
