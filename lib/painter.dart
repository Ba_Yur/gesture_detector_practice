import 'package:flutter/material.dart';


class SliderPainter extends CustomPainter {

  final double deltaX;
  final double containerHeight;
  final double sliderWidth;

  SliderPainter(this.deltaX, this.containerHeight, this.sliderWidth);

  @override
  void paint(Canvas canvas, Size size) {
    final myPainter = Paint()
    ..color = Colors.red
    ..strokeWidth = 5;

    final subLine = Paint()
      ..color = Colors.red
      ..strokeWidth = 1;

    canvas.drawLine(Offset(0, containerHeight/2), Offset(deltaX, containerHeight/2), myPainter);
    canvas.drawLine(Offset(0, containerHeight/2), Offset(sliderWidth, containerHeight/2), subLine);
    canvas.drawCircle(Offset(deltaX, containerHeight/2), 10, myPainter);

  }

  @override
  bool shouldRepaint(covariant SliderPainter oldDelegate) {
    return deltaX != oldDelegate.deltaX;
  }

  
}